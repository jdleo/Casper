//
//  InboxVC.swift
//  Casper
//
//  Created by John Leonardo on 1/15/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase

class InboxVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //empty messages array
    var messages: [[String: Any]] = []
    
    //for image cache
    let imageCache = NSCache<NSString, UIImage>()
    
    //for selected user rows
    var selectedUserId = ""
    var selectedUserName = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //gradient view
        let gradient = CAGradientLayer()
        gradient.frame = topView.bounds
        gradient.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        topView.layer.insertSublayer(gradient, at: 0)
        //gradient top view
        let gradient2 = CAGradientLayer()
        gradient2.frame = view.bounds
        gradient2.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient2.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient2.endPoint = CGPoint(x: 1.0, y: 0.5)
        view.layer.insertSublayer(gradient2, at: 0)
        
        //set delegate + datasource
        tableView.delegate = self
        tableView.dataSource = self
        
        //listen
        let db = Firestore.firestore()
        let conversationRef = db.collection("conversations").document(Auth.auth().currentUser!.uid).collection("conversations").order(by: "timestamp", descending: true)
        conversationRef.addSnapshotListener { (snapshot, error) in
            if let err = error {
                print(err.localizedDescription)
            } else {
                self.messages = []
                for document in snapshot!.documents {
                    self.messages.append([
                        "username": document.data()["username"] as! String,
                        "timestamp": document.data()["timestamp"] as! Timestamp,
                        "lastMessage": document.data()["lastMessage"] as! String,
                        "userId": document.documentID
                    ])
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChat1" {
            let destination = segue.destination as! UINavigationController
            let svc = destination.topViewController as! ChatVC
            svc.theirUserId = self.selectedUserId
            svc.theirUsername = self.selectedUserName
            svc.theirAvatar = imageCache.object(forKey: self.selectedUserId as NSString)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedUserName = self.messages[indexPath.row]["username"] as! String
        self.selectedUserId = self.messages[indexPath.row]["userId"] as! String
        UserDefaults.standard.set(Timestamp.init().seconds, forKey: "LO_" + selectedUserId)
        self.performSegue(withIdentifier: "goToChat1", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell", for: indexPath) as! InboxCell
        cell.avatarImg.image = UIImage(named: "DefaultAvatar1")
        
        //for async images
        cell.tag = indexPath.row
        
        //set up shadows on avatar content view
        cell.avatarContentView.clipsToBounds = false
        cell.avatarContentView.layer.cornerRadius = 8
        cell.avatarContentView.layer.shadowColor = UIColor.black.cgColor
        cell.avatarContentView.layer.shadowOpacity = 0.5
        cell.avatarContentView.layer.shadowOffset = CGSize.zero
        cell.avatarContentView.layer.shadowRadius = 8
        cell.avatarContentView.layer.shadowPath = UIBezierPath(roundedRect: cell.avatarContentView.bounds, cornerRadius: 8).cgPath
        
        //fill content view with avatar image
        cell.avatarImg.clipsToBounds = true
        cell.avatarImg.layer.cornerRadius = 8
        
        //set cell contents
        let uid = self.messages[indexPath.row]["userId"]! as! String
        let timestamp = self.messages[indexPath.row]["timestamp"]! as! Timestamp
        cell.usernameLbl.text = "@" + (self.messages[indexPath.row]["username"]! as! String)
        cell.previewLbl.text = self.messages[indexPath.row]["lastMessage"]! as! String
        cell.timeLbl.text = self.timeAgo(timestamp: timestamp)
        
        //check if unread
        if (UserDefaults.standard.integer(forKey: "LO_" + uid) >= timestamp.seconds) {
            cell.unreadView.isHidden = true
            //print("seen")
        } else {
            cell.unreadView.layer.cornerRadius = cell.unreadView.frame.size.width / 2
            cell.unreadView.clipsToBounds = true
            cell.unreadView.isHidden = false
            //print("unseen")
        }
        
        //attempt to load image from cache
        if let imageFromCache = imageCache.object(forKey: uid as NSString) {
            cell.avatarImg.image = imageFromCache
        } else {
            //attempt to download from storage
            let storage = Storage.storage().reference(withPath: "avatars/" + uid)
            storage.getData(maxSize: 5 * 1024 * 1024) { data, error in
                if let error = error {
                    //probably doesn't exist, load random default profile pic
                    let num = Int.random(in: 1...12)
                    cell.avatarImg.image = UIImage(named: "DefaultAvatar\(num)")
                    self.imageCache.setObject(UIImage(named: "DefaultAvatar\(num)")!, forKey: uid as NSString)
                } else {
                    // Data is returned
                    DispatchQueue.main.async(execute: {() -> Void in
                        if cell.tag == indexPath.row {
                            cell.avatarImg.image = UIImage(data: data!)
                            self.imageCache.setObject(UIImage(data: data!)!, forKey: uid as NSString)
                        }
                    })
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //fixed height of 80
        return 80
    }
    
    func timeAgo(timestamp: Timestamp) -> String {
        let now = Timestamp.init().seconds
        let secondsAgo = now - timestamp.seconds
        let minute: Int64 = 60
        let hour: Int64 = 60 * minute
        let day: Int64 = 24 * hour
        let week: Int64 = 7 * day
        
        if secondsAgo < 15 {
            return "Moments ago"
        }
        
        if secondsAgo < minute {
            return "\(secondsAgo) seconds ago"
        }
            
        else if secondsAgo < hour {
            return "\(secondsAgo / minute) minutes ago"
        }
        else if secondsAgo < day {
            return "\(secondsAgo / hour) hours ago"
        }
        else if secondsAgo < week {
            return "\(secondsAgo / day) days ago"
        }
        return "\(secondsAgo / week) weeks ago"
    }

}
