//
//  UserModal.swift
//  Casper
//
//  Created by John Leonardo on 2/3/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase
import SwiftEntryKit

class UserModal: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var msgButton: UIButton!
    @IBOutlet weak var usernameLbl: UILabel!
    
    //for referencing the current user we're trying to view
    var userId: String!
    var userImg: UIImage?
    var username: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //semi transparent background
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        //round bottom corners of content view
        contentView.roundCorners([.topLeft, .topRight], radius: 20)
        
        //style avatar image view
        avatarImg.layer.borderWidth = 3
        avatarImg.layer.masksToBounds = false
        avatarImg.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
        avatarImg.layer.cornerRadius = avatarImg.frame.height / 2
        avatarImg.clipsToBounds = true
        avatarImg.image = UIImage(named: "DefaultAvatar1")
        
        //set up swipe gesture
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        //style button views
        addButton.layer.cornerRadius = 15
        msgButton.layer.cornerRadius = 15
        
        //load avatar img
        if let userImg = self.userImg {
            avatarImg.image = userImg
        }
        
        //attempt to get username
        let db = Firestore.firestore()
        let usernameRef = db.collection("usernames").document(userId)
        usernameRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let username = document.data()!["username"] as! String
                self.username = username
                self.usernameLbl.text = "@" + username
            } else {
                print("Document does not exist")
            }
        }
    }
    
    @IBAction func addFriend(_ sender: Any) {
        let db = Firestore.firestore()
        if userId == Auth.auth().currentUser!.uid {
            self.showNotification(title: "Oops", message: "You can't add yourself. 🤔")
        } else {
            db.collection("graph").whereField(userId, isEqualTo: true).whereField(Auth.auth().currentUser!.uid, isEqualTo: true).getDocuments { (snapshot, error) in
                if let err = error {
                    //doesn't exist?
                    print("Error getting documents: \(err.localizedDescription)")
                } else {
                    //exists
                    if snapshot!.documents.count == 0 {
                        //make friend request
                        if let username = self.username, let uid = Auth.auth().currentUser?.uid {
                            //attempt to add this user
                            let db = Firestore.firestore()
                            let graphRef = db.collection("graph").document(uid + self.userId)
                            let data = [
                                uid: true,
                                self.userId: true,
                                "accepted": false,
                                "pending": true,
                                "toUsername": username,
                                "fromUsername": UserDefaults.standard.string(forKey: "username") ?? "casper"
                                ] as [String : Any]
                            
                            graphRef.setData(data, completion: { (error) in
                                if let err = error {
                                    self.showNotification(title: "Error", message: err.localizedDescription)
                                } else {
                                    self.showNotification(title: "Success", message: "Friend request sent to @\(username)")
                                }
                            })
                        }
                    } else {
                        let document = snapshot!.documents[0]
                        if !(document.data()["accepted"] as! Bool) {
                            self.showNotification(title: "Oops", message: "This friend request is already pending.")
                        } else {
                            self.showNotification(title: "Oops", message: "This user is already your friend.")
                        }
                    }
                    
                }
            }
        }
        
    }
    
    @IBAction func attemptMessage(_ sender: Any) {
        if Auth.auth().currentUser!.uid == userId {
            self.showNotification(title: "Oops", message: "You can't message yourself. 🤔")
        } else {
            //good to go
            let db = Firestore.firestore()
            db.collection("graph").whereField(userId, isEqualTo: true).whereField(Auth.auth().currentUser!.uid, isEqualTo: true).whereField("accepted", isEqualTo: true).getDocuments { (snapshot, error) in
                if let err = error {
                    //doesn't exist?
                    print("Error getting documents: \(err.localizedDescription)")
                } else {
                    //exists
                    if snapshot!.documents.count == 0 {
                        //no documents exists with these attributes ... (not friends yet)
                        self.showNotification(title: "Oops", message: "You're not friends with this user yet. You can only message people you're friends with.")
                    } else {
                        //USERS ARE FRIENDS, ALLOWED TO MESSAGE!
                        self.performSegue(withIdentifier: "goToChat3", sender: nil)
                    }
                    
                }
            }
        }
    }
    
    @IBAction func reportUser(_ sender: Any) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose a reason for reporting this user.", preferredStyle: .actionSheet)
        optionMenu.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { (action) in
            let db = Firestore.firestore()
            db.collection("reports").addDocument(data: [
                "reason": "nudity",
                "reportedBy": Auth.auth().currentUser!.uid,
                "offendingUser": self.userId]
            )
            self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Illegal Content", style: .default, handler: { (action) in
            let db = Firestore.firestore()
            db.collection("reports").addDocument(data: [
                "reason": "illegal content",
                "reportedBy": Auth.auth().currentUser!.uid,
                "offendingUser": self.userId]
            )
            self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Harassment", style: .default, handler: { (action) in
            let db = Firestore.firestore()
            db.collection("reports").addDocument(data: [
                "reason": "harassment",
                "reportedBy": Auth.auth().currentUser!.uid,
                "offendingUser": self.userId]
            )
            self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Catfishing", style: .default, handler: { (action) in
            let db = Firestore.firestore()
            db.collection("reports").addDocument(data: [
                "reason": "catfishing",
                "reportedBy": Auth.auth().currentUser!.uid,
                "offendingUser": self.userId]
            )
            self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Other", style: .default, handler: { (action) in
            let db = Firestore.firestore()
            db.collection("reports").addDocument(data: [
                "reason": "other",
                "reportedBy": Auth.auth().currentUser!.uid,
                "offendingUser": self.userId]
            )
            self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @objc func handleGesture() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showNotification(title: String, message: String) {
        var attributes = EKAttributes.topFloat
        attributes.entryBackground = .gradient(gradient: .init(colors: [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 15, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.roundCorners = .all(radius: 15)
        attributes.displayDuration = 4
        let minEdge = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: minEdge), height: .intrinsic)
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont(name: "CircularStd-Bold", size: 15)!, color: UIColor.white))
        let description = EKProperty.LabelContent(text: message, style: .init(font: UIFont(name: "CircularStd-Book", size: 14)!, color: UIColor.white))
        let image = EKProperty.ImageContent(image: UIImage(named: "CasperIcon")!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChat3" {
            let destination = segue.destination as! UINavigationController
            let svc = destination.topViewController as! ChatVC
            svc.theirUserId = self.userId
            if let username = self.username, let image = self.userImg {
                svc.theirAvatar = image
                svc.theirUsername = username
            } else {
                svc.theirUsername = "Error"
                svc.theirAvatar = UIImage(named: "DefaultAvatar1")
            }
        }
    }

}
