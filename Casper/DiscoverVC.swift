//
//  DiscoverVC.swift
//  Casper
//
//  Created by John Leonardo on 1/15/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase

class DiscoverVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //for image cache
    let imageCache = NSCache<NSString, UIImage>()
    
    //user collection
    var users: [String] = []
    
    //for pull to refresh
    var refreshControl = UIRefreshControl()
    
    //for selected user in cell
    var selectedIndexPath = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegate + datasource for collection
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //cell spacing
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.
        
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        flow.minimumInteritemSpacing = 2
        flow.minimumLineSpacing = 2
        
        //set up refresh
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        collectionView.refreshControl = refreshControl

        //gradient view
        let gradient = CAGradientLayer()
        gradient.frame = topView.bounds
        gradient.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        topView.layer.insertSublayer(gradient, at: 0)
        //gradient top view
        let gradient2 = CAGradientLayer()
        gradient2.frame = view.bounds
        gradient2.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient2.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient2.endPoint = CGPoint(x: 1.0, y: 0.5)
        view.layer.insertSublayer(gradient2, at: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.downloadUsers()
    }
    
    @IBAction func openSettings(_ sender: Any) {
        self.performSegue(withIdentifier: "goToOptionsModal", sender: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath.row
        self.performSegue(withIdentifier: "goToUserModal", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCell", for: indexPath) as! UserCell
        
        //set up shadows on avatar content view
        cell.innerContentView.clipsToBounds = false
        cell.innerContentView.layer.cornerRadius = 8
        cell.innerContentView.layer.shadowColor = UIColor.black.cgColor
        cell.innerContentView.layer.shadowOpacity = 0.5
        cell.innerContentView.layer.shadowOffset = CGSize.zero
        cell.innerContentView.layer.shadowRadius = 6
        cell.innerContentView.layer.shadowPath = UIBezierPath(roundedRect: cell.innerContentView.bounds, cornerRadius: 8).cgPath
        
        //fill content view with avatar image
        cell.avatarImg.clipsToBounds = true
        cell.avatarImg.layer.cornerRadius = 8
        
        //attempt to load image from cache
        if let imageFromCache = imageCache.object(forKey: self.users[indexPath.item] as NSString) {
            cell.avatarImg.image = imageFromCache
        } else {
            //attempt to download from storage
            let storage = Storage.storage().reference(withPath: "avatars/" + self.users[indexPath.item])
            storage.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    //probably doesn't exist, load random default profile pic
                    let num = Int.random(in: 1...12)
                    cell.avatarImg.image = UIImage(named: "DefaultAvatar\(num)")
                    self.imageCache.setObject(UIImage(named: "DefaultAvatar\(num)")!, forKey: self.users[indexPath.item] as NSString)
                } else {
                    // Data for "images/island.jpg" is returned
                    cell.avatarImg.image = UIImage(data: data!)
                    self.imageCache.setObject(UIImage(data: data!)!, forKey: self.users[indexPath.item] as NSString)
                }
            }
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToUserModal" {
            //attempting to open user modal
            let destination = segue.destination as! UserModal
            destination.userId = self.users[selectedIndexPath]
            if let image = imageCache.object(forKey: self.users[selectedIndexPath] as NSString) {
                destination.userImg = image
            }
        }
    }
    
    func downloadUsers() {
        var g = UserDefaults.standard.integer(forKey: "gender")
        let f = UserDefaults.standard.integer(forKey: "filter")
        
        let db = Firestore.firestore()
        let ref = db.collection("recents")
        
        if f == 0 {
            if g == 1 || g == 2 {
                //recent boy/girl
                self.users = []
                
                if g == 2 {
                    g = 0
                } else {
                    g = 1
                }
                
                let query = ref.whereField("g", isEqualTo: g).whereField("b", isEqualTo: false).order(by: "timestamp", descending: true).limit(to: 100)
                query.getDocuments { (snapshot, error) in
                    if let err = error {
                        print(err)
                    } else {
                        if let snapshot = snapshot {
                            for snap in snapshot.documents {
                                self.users.append(snap.documentID)
                            }
                            self.collectionView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                }
            } else {
                //recent both
                self.users = []
                let query = ref.whereField("b", isEqualTo: false).order(by: "timestamp", descending: true).limit(to: 100)
                query.getDocuments { (snapshot, error) in
                    if let err = error {
                        print(err)
                    } else {
                        if let snapshot = snapshot {
                            for snap in snapshot.documents {
                                self.users.append(snap.documentID)
                            }
                            self.collectionView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                }
            }
        } else {
            if g == 1 || g == 2 {
                //random boy/girl
                
                if g == 2 {
                    g = 0
                } else {
                    g = 1
                }
                
                self.users = []
                
                let num = Int.random(in: Int.min ... Int.max)
                if num < 0 {
                    let query = ref.whereField("g", isEqualTo: g).whereField("b", isEqualTo: false).whereField("r", isGreaterThan: num).limit(to: 100)
                    query.getDocuments { (snapshot, error) in
                        if let err = error {
                            print(err)
                        } else {
                            if let snapshot = snapshot {
                                for snap in snapshot.documents {
                                    self.users.append(snap.documentID)
                                }
                                self.collectionView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    }
                } else {
                    let query = ref.whereField("g", isEqualTo: g).whereField("b", isEqualTo: false).whereField("r", isLessThan: num).limit(to: 100)
                    query.getDocuments { (snapshot, error) in
                        if let err = error {
                            print(err)
                        } else {
                            if let snapshot = snapshot {
                                for snap in snapshot.documents {
                                    self.users.append(snap.documentID)
                                }
                                self.collectionView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    }
                }
            } else {
                //random both
                self.users = []
                
                let num = Int.random(in: Int.min ... Int.max)
                if num < 0 {
                    let query = ref.whereField("b", isEqualTo: false).whereField("r", isGreaterThan: num).limit(to: 100)
                    query.getDocuments { (snapshot, error) in
                        if let err = error {
                            print(err)
                        } else {
                            if let snapshot = snapshot {
                                for snap in snapshot.documents {
                                    self.users.append(snap.documentID)
                                }
                                self.collectionView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    }
                } else {
                    let query = ref.whereField("b", isEqualTo: false).whereField("r", isLessThan: num).limit(to: 100)
                    query.getDocuments { (snapshot, error) in
                        if let err = error {
                            print(err)
                        } else {
                            if let snapshot = snapshot {
                                for snap in snapshot.documents {
                                    self.users.append(snap.documentID)
                                }
                                self.collectionView.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func refreshData() {
        self.downloadUsers()
    }

}

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
            layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
}
