//
//  FriendsVC.swift
//  Casper
//
//  Created by John Leonardo on 1/15/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase
import SwiftEntryKit

class FriendsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //data arrays
    var outgoingFriends: [[String: String]] = []
    var incomingFriends: [[String: String]] = []
    var friends: [[String: String]] = []
    
    //for image cache
    let imageCache = NSCache<NSString, UIImage>()
    
    //for selected user rows
    var selectedUserId = ""
    var selectedUserName = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tableview setup
        tableView.delegate = self
        tableView.dataSource = self

        //gradient view
        let gradient = CAGradientLayer()
        gradient.frame = topView.bounds
        gradient.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        topView.layer.insertSublayer(gradient, at: 0)
        //gradient top view
        let gradient2 = CAGradientLayer()
        gradient2.frame = view.bounds
        gradient2.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient2.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient2.endPoint = CGPoint(x: 1.0, y: 0.5)
        view.layer.insertSublayer(gradient2, at: 0)
        
        //download friend data
        self.downloadFriends()
    }
    
    @IBAction func add(_ sender: Any) {
        let alertController = UIAlertController(title: "Add Friends", message: "Type the @username of the friend you want to add below.", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Username"
        }
        let addAction = UIAlertAction(title: "Add", style: .default, handler: { alert -> Void in
            let usernameTextField = alertController.textFields![0] as UITextField
            
            if usernameTextField.text != "" {
                let db = Firestore.firestore()
                let usernameQuery = db.collection("usernames").whereField("username", isEqualTo: usernameTextField.text!.lowercased())
                usernameQuery.getDocuments(completion: { (snapshot, error) in
                    if let err = error {
                        self.showNotification(title: "Error", message: err.localizedDescription)
                    } else if let res = snapshot {
                        if res.documents.count == 0 {
                            //no results, means username doesn't exist
                            self.showNotification(title: "Oops", message: "@\(usernameTextField.text!) doesn't exist. 😔")
                        } else {
                            //username exists
                            let user = res.documents[0]
                            let userId = user.documentID
                            if let username = user.data()["username"] as? String, let uid = Auth.auth().currentUser?.uid {
                                //attempt to add this user
                                let db = Firestore.firestore()
                                let graphRef = db.collection("graph").document(uid + userId)
                                let data = [
                                    uid: true,
                                    userId: true,
                                    "accepted": false,
                                    "pending": true,
                                    "toUsername": username,
                                    "fromUsername": UserDefaults.standard.string(forKey: "username") ?? "casper"
                                    ] as [String : Any]
                                
                                graphRef.setData(data, completion: { (error) in
                                    if let err = error {
                                        self.showNotification(title: "Error", message: err.localizedDescription)
                                    } else {
                                        self.showNotification(title: "Success", message: "Friend request sent to @\(usernameTextField.text!)")
                                    }
                                })
                            }
                        }
                    }
                })
            } else {
                self.showNotification(title: "Error", message: "Text field can't be empty.")
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showNotification(title: String, message: String) {
        var attributes = EKAttributes.topFloat
        attributes.entryBackground = .gradient(gradient: .init(colors: [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 15, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.roundCorners = .all(radius: 15)
        attributes.displayDuration = 2
        let minEdge = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: minEdge), height: .intrinsic)
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont(name: "CircularStd-Bold", size: 15)!, color: UIColor.white))
        let description = EKProperty.LabelContent(text: message, style: .init(font: UIFont(name: "CircularStd-Book", size: 14)!, color: UIColor.white))
        let image = EKProperty.ImageContent(image: UIImage(named: "CasperIcon")!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            //incoming
            return incomingFriends.count
        } else if section == 1 {
            //outgoing
            return outgoingFriends.count
        } else {
            //existing
            return friends.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
        view.backgroundColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0)
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: 30))
        label.font = UIFont(name: "CircularStd-Bold", size: 16)
        label.textColor = UIColor.white
        if section == 0 {
            label.text = "Incoming friend requests"
        } else if section == 1 {
            label.text = "Outgoing friend requests"
        } else {
            label.text = "Friends"
        }
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! InboxCell
        
        //string for userid
        var rowUserId = ""
        
        //set up shadows on avatar content view
        cell.avatarContentView.clipsToBounds = false
        cell.avatarContentView.layer.cornerRadius = 8
        cell.avatarContentView.layer.shadowColor = UIColor.black.cgColor
        cell.avatarContentView.layer.shadowOpacity = 0.5
        cell.avatarContentView.layer.shadowOffset = CGSize.zero
        cell.avatarContentView.layer.shadowRadius = 8
        cell.avatarContentView.layer.shadowPath = UIBezierPath(roundedRect: cell.avatarContentView.bounds, cornerRadius: 8).cgPath
        
        //fill content view with avatar image
        cell.avatarImg.clipsToBounds = true
        cell.avatarImg.layer.cornerRadius = 8
        
        if indexPath.section == 0 {
            cell.usernameLbl.text = "@" + self.incomingFriends[indexPath.row]["username"]!
            rowUserId = self.incomingFriends[indexPath.row]["id"]!.replacingOccurrences(of: Auth.auth().currentUser!.uid, with: "")
        } else if indexPath.section == 1 {
            cell.usernameLbl.text = "@" + self.outgoingFriends[indexPath.row]["username"]!
            rowUserId = self.outgoingFriends[indexPath.row]["id"]!.replacingOccurrences(of: Auth.auth().currentUser!.uid, with: "")
        } else {
            cell.usernameLbl.text = "@" + self.friends[indexPath.row]["username"]!
            rowUserId = self.friends[indexPath.row]["id"]!.replacingOccurrences(of: Auth.auth().currentUser!.uid, with: "")
        }
        
        //attempt to load image from cache
        if let imageFromCache = imageCache.object(forKey: rowUserId as NSString) {
            cell.avatarImg.image = imageFromCache
        } else {
            //attempt to download from storage
            let storage = Storage.storage().reference(withPath: "avatars/" + rowUserId)
            storage.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    //probably doesn't exist, load random default profile pic
                    let num = Int.random(in: 1...12)
                    cell.avatarImg.image = UIImage(named: "DefaultAvatar\(num)")
                    self.imageCache.setObject(UIImage(named: "DefaultAvatar\(num)")!, forKey: rowUserId as NSString)
                } else {
                    // Data for "images/island.jpg" is returned
                    cell.avatarImg.image = UIImage(data: data!)
                    self.imageCache.setObject(UIImage(data: data!)!, forKey: rowUserId as NSString)
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //fixed height of 80
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            //incoming
            let optionMenu = UIAlertController(title: nil, message: "Options", preferredStyle: .actionSheet)
            optionMenu.addAction(UIAlertAction(title: "Accept", style: .default, handler: { (action) in
                //get document id
                let documentId = self.incomingFriends[indexPath.row]["id"]!
                
                Firestore.firestore().collection("graph").document(documentId).updateData(["pending": false, "accepted": true], completion: { (error) in
                    if error != nil {
                        return
                    }
                    self.downloadFriends()
                })
                
            }))
            optionMenu.addAction(UIAlertAction(title: "Decline", style: .destructive, handler: { (action) in
                //get document id
                let documentId = self.incomingFriends[indexPath.row]["id"]!
                
                //destroy document
                Firestore.firestore().collection("graph").document(documentId).delete(completion: { (error) in
                    if error != nil {
                        return
                    }
                    self.downloadFriends()
                })
            }))
            optionMenu.addAction(UIAlertAction(title: "Quit", style: .cancel, handler: nil))
            self.present(optionMenu, animated: true, completion: nil)
        } else if indexPath.section == 1 {
            //outgoing
            let optionMenu = UIAlertController(title: nil, message: "Options", preferredStyle: .actionSheet)
            optionMenu.addAction(UIAlertAction(title: "Cancel Friend Request", style: .destructive, handler: { (action) in
                //get document id
                let documentId = self.outgoingFriends[indexPath.row]["id"]!
                
                //destroy document
                Firestore.firestore().collection("graph").document(documentId).delete(completion: { (error) in
                    if error != nil {
                        return
                    }
                    self.downloadFriends()
                })
            }))
            optionMenu.addAction(UIAlertAction(title: "Quit", style: .cancel, handler: nil))
            self.present(optionMenu, animated: true, completion: nil)
        } else {
            let optionMenu = UIAlertController(title: nil, message: "Options", preferredStyle: .actionSheet)
            optionMenu.addAction(UIAlertAction(title: "Message", style: .default, handler: { (action) in
                //go to chat VC
                self.selectedUserId = self.friends[indexPath.row]["id"]!.replacingOccurrences(of: Auth.auth().currentUser!.uid, with: "")
                self.selectedUserName = self.friends[indexPath.row]["username"]!
                self.performSegue(withIdentifier: "goToChat2", sender: nil)
            }))
            optionMenu.addAction(UIAlertAction(title: "Remove Friend", style: .destructive, handler: { (action) in
                //get document id
                let documentId = self.friends[indexPath.row]["id"]!
                
                //destroy document
                Firestore.firestore().collection("graph").document(documentId).delete(completion: { (error) in
                    if error != nil {
                        return
                    }
                    self.downloadFriends()
                })
            }))
            optionMenu.addAction(UIAlertAction(title: "Quit", style: .cancel, handler: nil))
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func downloadFriends() {
        //clear data arrays first to prevent overlapping of data in table view :)
        self.incomingFriends = []
        self.outgoingFriends = []
        self.friends = []
        
        //firestore reference
        let db = Firestore.firestore()
        db.collection("graph").whereField(Auth.auth().currentUser!.uid, isEqualTo: true).getDocuments { (snapshot, error) in
            if let err = error {
                print(err.localizedDescription)
            } else if let snapshot = snapshot {
                for document in snapshot.documents {
                    if document.data()["accepted"] as! Bool {
                        //is friend
                        if document.documentID.starts(with: Auth.auth().currentUser!.uid) {
                            self.friends.append([
                                "username": document.data()["toUsername"] as! String,
                                "id": document.documentID
                                ])
                        } else {
                            self.friends.append([
                                "username": document.data()["fromUsername"] as! String,
                                "id": document.documentID
                                ])
                        }
                        
                    } else {
                        if document.data()["pending"] as! Bool {
                            if document.documentID.starts(with: Auth.auth().currentUser!.uid) {
                                //outgoing friend request
                                self.outgoingFriends.append([
                                    "username": document.data()["toUsername"] as! String,
                                    "id": document.documentID
                                    ])
                            } else {
                                //incoming friend request
                                self.incomingFriends.append([
                                    "username": document.data()["fromUsername"] as! String,
                                    "id": document.documentID
                                    ])
                            }
                        }
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChat2" {
            let destination = segue.destination as! UINavigationController
            let svc = destination.topViewController as! ChatVC
            svc.theirUserId = self.selectedUserId
            svc.theirUsername = self.selectedUserName
            svc.theirAvatar = imageCache.object(forKey: self.selectedUserId as NSString)
            
        }
    }

}
