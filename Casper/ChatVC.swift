//
//  ChatVC.swift
//  Casper
//
//  Created by John Leonardo on 2/3/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import MessengerKit
import Firebase
import SwiftEntryKit

class ChatVC: MSGMessengerViewController {
    var theirUsername: String!
    var theirUserId: String!
    var theirAvatar: UIImage!
    
    var me = User(displayName: UserDefaults.standard.object(forKey: "username") as! String, avatar: UIImage(named: "DefaultAvatar6")!, avatarUrl: nil, isSender: true)
    
    var them = User(displayName: "default", avatar: UIImage(named: "DefaultAvatar1")!, avatarUrl: nil, isSender: false)
    
    //reference to firestore listener that we will use
    var listener: ListenerRegistration!
    
    var id = 100
    
    override var style: MSGMessengerStyle {
        var style = MessengerKit.Styles.travamigos
        style.outgoingGradient = [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor, UIColor(red:0.40, green:0.40, blue:0.91, alpha:1.0).cgColor, UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor]
        return style
    }
    
    
    var seen: [String] = []
    
    lazy var messages: [[MSGMessage]] = {
        return []
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Chat"
        
        dataSource = self
        delegate = self
        
        //set user info
        them.displayName = self.theirUsername
        them.avatar = self.theirAvatar
        
        //download messages
        listener = self.downloadMessages()
        
        //attempt to download from storage
        let storage = Storage.storage().reference(withPath: "avatars/" + Auth.auth().currentUser!.uid)
        storage.getData(maxSize: 5 * 1024 * 1024) { data, error in
            if let error = error {
                //probably doesn't exist, load random default profile pic
                let num = Int.random(in: 1...12)
                self.me.avatar = UIImage(named: "DefaultAvatar\(num)")
            } else {
                // Data for "images/island.jpg" is returned
                self.me.avatar = UIImage(data: data!)
            }
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        collectionView.scrollToBottom(animated: false)
        
    }
    
    @IBAction func report(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Thanks for helping keep this community safe. Please choose an option below!", preferredStyle: .actionSheet)
        optionMenu.addAction(UIAlertAction(title: "Report", style: .default, handler: { (action) in
            let optionMenu2 = UIAlertController(title: nil, message: "Choose a reason for reporting this user.", preferredStyle: .actionSheet)
            optionMenu2.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { (action) in
                let db = Firestore.firestore()
                db.collection("reports").addDocument(data: [
                    "reason": "nudity",
                    "reportedBy": Auth.auth().currentUser!.uid,
                    "offendingUser": self.theirUserId]
                )
                self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
            }))
            
            optionMenu2.addAction(UIAlertAction(title: "Illegal Content", style: .default, handler: { (action) in
                let db = Firestore.firestore()
                db.collection("reports").addDocument(data: [
                    "reason": "illegal content",
                    "reportedBy": Auth.auth().currentUser!.uid,
                    "offendingUser": self.theirUserId]
                )
                self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
            }))
            
            optionMenu2.addAction(UIAlertAction(title: "Harassment", style: .default, handler: { (action) in
                let db = Firestore.firestore()
                db.collection("reports").addDocument(data: [
                    "reason": "harassment",
                    "reportedBy": Auth.auth().currentUser!.uid,
                    "offendingUser": self.theirUserId]
                )
                self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
            }))
            
            optionMenu2.addAction(UIAlertAction(title: "Catfishing", style: .default, handler: { (action) in
                let db = Firestore.firestore()
                db.collection("reports").addDocument(data: [
                    "reason": "catfishing",
                    "reportedBy": Auth.auth().currentUser!.uid,
                    "offendingUser": self.theirUserId]
                )
                self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
            }))
            
            optionMenu2.addAction(UIAlertAction(title: "Other", style: .default, handler: { (action) in
                let db = Firestore.firestore()
                db.collection("reports").addDocument(data: [
                    "reason": "other",
                    "reportedBy": Auth.auth().currentUser!.uid,
                    "offendingUser": self.theirUserId]
                )
                self.showNotification(title: "Success!", message: "Your report has been submitted. Thanks for helping keep this community safe. If we need further information, we will reach out to you in Support Chat.")
            }))
            
            optionMenu2.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            
            self.present(optionMenu2, animated: true, completion: nil)
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Block", style: .destructive, handler: { (action) in
            let db = Firestore.firestore()
            db.collection("graph").document(self.theirUserId + Auth.auth().currentUser!.uid).delete(completion: { (error) in
                if error != nil {
                    //might be another reference
                    db.collection("graph").document(Auth.auth().currentUser!.uid + self.theirUserId).delete(completion: { (error) in
                        if error != nil {
                            print(error?.localizedDescription)
                        } else {
                            //success, show message and destroy view
                            self.cleanUp()
                        }
                    })
                } else {
                    //success, show message and destroy view
                    self.cleanUp()
                }
            })
            
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func cleanUp() {
        // Get new write batch
        let db = Firestore.firestore()
        let batch = db.batch()
        
        // Delete references of conversations
        let ref1 = db.collection("conversations").document(self.theirUserId).collection("conversations").document(Auth.auth().currentUser!.uid)
        batch.deleteDocument(ref1)
        
        let ref2 = db.collection("conversations").document(Auth.auth().currentUser!.uid).collection("conversations").document(self.theirUserId)
        batch.deleteDocument(ref2)
        
        // Commit the batch
        batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                self.showNotification(title: "Success!", message: "This user has been blocked. If you also wanted to file a report but forgot to, feel free to reach out to us in Support Chat!")
                self.dismiss(animated: true) {
                    self.messages = []
                    self.listener?.remove()
                    self.navigationController!.viewControllers.removeAll()
                }
            }
        }
    }
    
    override func inputViewPrimaryActionTriggered(inputView: MSGInputView) {
        
        //for unreads
        UserDefaults.standard.set(Timestamp.init().seconds, forKey: "LO_" + self.theirUserId)
        
        
        let db = Firestore.firestore()
        var participants: [String] = [Auth.auth().currentUser!.uid, theirUserId]
        participants = participants.sorted()
        let conversationId = participants.joined(separator: "")
        
        //Get new write batch
        let batch = db.batch()
        let timestamp = Timestamp.init()
        
        // Set the value of 'NYC'
        let chatRef = db.collection("messages").document(conversationId).collection("messages").document()
        batch.setData([
            "body": inputView.message,
            "timestamp": timestamp,
            "sender": Auth.auth().currentUser!.uid
        ], forDocument: chatRef)
        
        // Update our conversation reference
        let myRef = db.collection("conversations").document(Auth.auth().currentUser!.uid).collection("conversations").document(self.theirUserId)
        batch.setData(["lastMessage": inputView.message, "timestamp": timestamp, "username": self.theirUsername], forDocument: myRef, merge: true)
        
        // Update their conversation reference
        let theirRef = db.collection("conversations").document(self.theirUserId).collection("conversations").document(Auth.auth().currentUser!.uid)
        batch.setData(["lastMessage": inputView.message, "timestamp": timestamp, "username": UserDefaults.standard.object(forKey: "username") as! String], forDocument: theirRef, merge: true)
        
        // Commit the batch
        batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                print("Batch write succeeded.")
            }
        }
        
        inputView.resignFirstResponder()
    }
    
    override func insert(_ message: MSGMessage) {
        
        collectionView.performBatchUpdates({
            if let lastSection = self.messages.last, let lastMessage = lastSection.last, lastMessage.user.displayName == message.user.displayName {
                self.messages[self.messages.count - 1].append(message)
                
                let sectionIndex = self.messages.count - 1
                let itemIndex = self.messages[sectionIndex].count - 1
                self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                
            } else {
                self.messages.append([message])
                let sectionIndex = self.messages.count - 1
                self.collectionView.insertSections([sectionIndex])
            }
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: true)
            self.collectionView.layoutTypingLabelIfNeeded()
        })
        
    }
    
    override func insert(_ messages: [MSGMessage], callback: (() -> Void)? = nil) {
        
        collectionView.performBatchUpdates({
            for message in messages {
                if let lastSection = self.messages.last, let lastMessage = lastSection.last, lastMessage.user.displayName == message.user.displayName {
                    self.messages[self.messages.count - 1].append(message)
                    
                    let sectionIndex = self.messages.count - 1
                    let itemIndex = self.messages[sectionIndex].count - 1
                    self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                    
                } else {
                    self.messages.append([message])
                    let sectionIndex = self.messages.count - 1
                    self.collectionView.insertSections([sectionIndex])
                }
            }
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: false)
            self.collectionView.layoutTypingLabelIfNeeded()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                callback?()
            }
        })
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true) {
            self.messages = []
            self.listener?.remove()
            self.navigationController!.viewControllers.removeAll()
        }
    }
    
    func showNotification(title: String, message: String) {
        var attributes = EKAttributes.topFloat
        attributes.entryBackground = .gradient(gradient: .init(colors: [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 15, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.roundCorners = .all(radius: 15)
        attributes.displayDuration = 2
        let minEdge = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: minEdge), height: .intrinsic)
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont(name: "CircularStd-Bold", size: 15)!, color: UIColor.white))
        let description = EKProperty.LabelContent(text: message, style: .init(font: UIFont(name: "CircularStd-Book", size: 14)!, color: UIColor.white))
        let image = EKProperty.ImageContent(image: UIImage(named: "CasperIcon")!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    func downloadMessages() -> ListenerRegistration {
        //get existing chat messages
        let db = Firestore.firestore()
        var participants: [String] = [Auth.auth().currentUser!.uid, theirUserId]
        participants = participants.sorted()
        let conversationId = participants.joined(separator: "")
        let chatRef = db.collection("messages").document(conversationId).collection("messages").order(by: "timestamp", descending: true).limit(to: 100)
        
        //listen for changes
        return chatRef.addSnapshotListener { (snapshot, error) in
            
            guard let documents = snapshot?.documents else {
                print("Error fetching documents: \(error!)")
                return
            }
            
            for document in documents.reversed() {
                if !self.seen.contains(document.documentID) {
                    self.id += 1
                    
                    let body = document.data()["body"] as! String
                    let sender = document.data()["sender"] as! String
                    let timestamp = document.data()["timestamp"] as! Timestamp
                    if body.containsOnlyEmoji {
                        //is emoji text
                        if sender == Auth.auth().currentUser!.uid {
                            //is self
                            self.insert(MSGMessage(id: self.id, body: .emoji(body), user: self.me, sentAt: timestamp.dateValue()))
                        } else {
                            //is casper
                            self.insert(MSGMessage(id: self.id, body: .emoji(body), user: self.them, sentAt: timestamp.dateValue()))
                        }
                    } else {
                        //is regular text
                        if sender == Auth.auth().currentUser!.uid {
                            //is self
                            self.insert(MSGMessage(id: self.id, body: .text(body), user: self.me, sentAt: timestamp.dateValue()))
                        } else {
                            //is casper
                            self.insert(MSGMessage(id: self.id, body: .text(body), user: self.them, sentAt: timestamp.dateValue()))
                        }
                    }
                    self.seen.append(document.documentID)
                }
            }
        }
    }
    
    
}

// MARK: - MSGDataSource
extension ChatVC: MSGDataSource {
    
    func numberOfSections() -> Int {
        return messages.count
    }
    
    func numberOfMessages(in section: Int) -> Int {
        return messages[section].count
    }
    
    func message(for indexPath: IndexPath) -> MSGMessage {
        return messages[indexPath.section][indexPath.item]
    }
    
    func footerTitle(for section: Int) -> String? {
        return "Just now"
    }
    
    func headerTitle(for section: Int) -> String? {
        return messages[section].first?.user.displayName
    }
    
}

// MARK: - MSGDelegate
extension ChatVC: MSGDelegate {
    
    func linkTapped(url: URL) {
        print("Link tapped:", url)
    }
    
    func avatarTapped(for user: MSGUser) {
        print("Avatar tapped:", user)
    }
    
    func tapReceived(for message: MSGMessage) {
        print("Tapped: ", message)
    }
    
    func longPressReceieved(for message: MSGMessage) {
        print("Long press:", message)
    }
    
    func shouldDisplaySafari(for url: URL) -> Bool {
        return true
    }
    
    func shouldOpen(url: URL) -> Bool {
        return true
    }
    
}
