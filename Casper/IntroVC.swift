//
//  IntroVC.swift
//  Casper
//
//  Created by John Leonardo on 1/15/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase
import NVActivityIndicatorView

class IntroVC: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var agreementLbl: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //gradient content view
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor, UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        view.layer.insertSublayer(gradient, at: 0)
        
        //style login btn
        loginBtn.clipsToBounds = false
        loginBtn.layer.cornerRadius = 15
        loginBtn.layer.shadowColor = UIColor.black.cgColor
        loginBtn.layer.shadowOpacity = 0.5
        loginBtn.layer.shadowOffset = CGSize.zero
        loginBtn.layer.shadowRadius = 15
        loginBtn.layer.shadowPath = UIBezierPath(roundedRect: loginBtn.bounds, cornerRadius: 15).cgPath
        
        //style segment
        segmentControl.layer.cornerRadius = 15
        
        //tap gesture for agreement label
        agreementLbl.isUserInteractionEnabled = true
        let agreementTap = UITapGestureRecognizer(target: self, action: #selector(agreementTapped))
        agreementLbl.addGestureRecognizer(agreementTap)
    }
    
    @IBAction func login(_ sender: Any) {
        if segmentControl.selectedSegmentIndex != -1 {
            startAnimating(CGSize(width: 30, height: 30), message: "Creating user in database...", type: NVActivityIndicatorType.allCases.randomElement())
            Auth.auth().signInAnonymously { (authResult, error) in
                if let err = error {
                    //error
                } else {
                    if let uid = authResult?.user.uid {
                        //log event in Analytics
                        Analytics.logEvent(AnalyticsEventSignUp, parameters: [
                            AnalyticsParameterMethod: "anonymous"
                            ])
                        
                        //create database reference
                        let db = Firestore.firestore()
                        
                        //generate random username
                        let username = Generator().username()
                        
                        //make sure username was successfully generated (?)
                        if username != "not" {
                            //create firestore batch
                            let batch = db.batch()
                            
                            //set user document
                            let userRef = db.collection("users").document(uid)
                            batch.setData(["username": username, "boosts": 3], forDocument: userRef)
                            
                            //set username document
                            let usernameRef = db.collection("usernames").document(uid)
                            batch.setData(["username": username], forDocument: usernameRef)
                            
                            //set recents document
                            let recentsRef = db.collection("recents").document(uid)
                            batch.setData(["timestamp": Timestamp.init(), "g": self.segmentControl.selectedSegmentIndex, "r": Int.random(in: Int.min ... Int.max), "b": false], forDocument: recentsRef)
                            
                            // Commit the batch
                            batch.commit() { err in
                                if let err = err {
                                    print("Error writing batch \(err.localizedDescription)")
                                } else {
                                    //do stuff here
                                    UserDefaults.standard.set(3, forKey: "boosts")
                                    UserDefaults.standard.set(self.segmentControl.selectedSegmentIndex, forKey: "myG")
                                    UserDefaults.standard.set(username, forKey: "username")
                                    self.stopAnimating()
                                    self.performSegue(withIdentifier: "goToMain", sender: nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func agreementTapped() {
        //do something
        guard let url = URL(string: "https://medium.com/@casperchat/terms-of-service-43b6d3e8c4fe") else { return }
        UIApplication.shared.open(url)
    }


}

