//
//  InboxCell.swift
//  Casper
//
//  Created by John Leonardo on 1/15/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var previewLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var avatarContentView: UIView!
    @IBOutlet weak var unreadView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
