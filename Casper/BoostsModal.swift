//
//  BoostsModal.swift
//  Casper
//
//  Created by John Leonardo on 2/3/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import SwiftEntryKit
import StoreKit

class BoostsModal: UIViewController, GADRewardBasedVideoAdDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if (response.products.count > 0) {
            //IAP product (consumable)
            iapProducts = response.products
            let product = response.products[0] as SKProduct
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .failed:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    if productId == "1" {
                        let db = Firestore.firestore()
                        let sfReference = db.collection("users").document(Auth.auth().currentUser!.uid)
                        
                        db.runTransaction({ (transaction, errorPointer) -> Any? in
                            let sfDocument: DocumentSnapshot
                            do {
                                try sfDocument = transaction.getDocument(sfReference)
                            } catch let fetchError as NSError {
                                errorPointer?.pointee = fetchError
                                return nil
                            }
                            
                            guard let oldBoosts = sfDocument.data()?["boosts"] as? Int else {
                                let error = NSError(
                                    domain: "AppErrorDomain",
                                    code: -1,
                                    userInfo: [
                                        NSLocalizedDescriptionKey: "Unable to retrieve population from snapshot \(sfDocument)"
                                    ]
                                )
                                errorPointer?.pointee = error
                                return nil
                            }
                            transaction.updateData(["boosts": oldBoosts + 15], forDocument: sfReference)
                            return nil
                        }) { (object, error) in
                            if let error = error {
                                print("Transaction failed: \(error)")
                            } else {
                                let newBoosts = UserDefaults.standard.integer(forKey: "boosts") + 15
                                UserDefaults.standard.set(newBoosts, forKey: "boosts")
                                self.showNotification(title: "Success!", message: "Thank you for supporting and purchasing \(15) boosts. You now have \(newBoosts) boosts! 🚀")
                                Analytics.logEvent("did_purchase", parameters: [
                                    AnalyticsParameterPrice: 0.99
                                ])
                            }
                        }
                    } else if productId == "2" {
                        let db = Firestore.firestore()
                        let sfReference = db.collection("users").document(Auth.auth().currentUser!.uid)
                        
                        db.runTransaction({ (transaction, errorPointer) -> Any? in
                            let sfDocument: DocumentSnapshot
                            do {
                                try sfDocument = transaction.getDocument(sfReference)
                            } catch let fetchError as NSError {
                                errorPointer?.pointee = fetchError
                                return nil
                            }
                            
                            guard let oldBoosts = sfDocument.data()?["boosts"] as? Int else {
                                let error = NSError(
                                    domain: "AppErrorDomain",
                                    code: -1,
                                    userInfo: [
                                        NSLocalizedDescriptionKey: "Unable to retrieve population from snapshot \(sfDocument)"
                                    ]
                                )
                                errorPointer?.pointee = error
                                return nil
                            }
                            transaction.updateData(["boosts": oldBoosts + 50], forDocument: sfReference)
                            return nil
                        }) { (object, error) in
                            if let error = error {
                                print("Transaction failed: \(error)")
                            } else {
                                let newBoosts = UserDefaults.standard.integer(forKey: "boosts") + 50
                                UserDefaults.standard.set(newBoosts, forKey: "boosts")
                                self.showNotification(title: "Success!", message: "Thank you for supporting and purchasing \(50) boosts. You now have \(newBoosts) boosts! 🚀")
                                Analytics.logEvent("did_purchase", parameters: [
                                    AnalyticsParameterPrice: 2.99
                                    ])
                            }
                        }
                    } else if productId == "3" {
                        let db = Firestore.firestore()
                        let sfReference = db.collection("users").document(Auth.auth().currentUser!.uid)
                        
                        db.runTransaction({ (transaction, errorPointer) -> Any? in
                            let sfDocument: DocumentSnapshot
                            do {
                                try sfDocument = transaction.getDocument(sfReference)
                            } catch let fetchError as NSError {
                                errorPointer?.pointee = fetchError
                                return nil
                            }
                            
                            guard let oldBoosts = sfDocument.data()?["boosts"] as? Int else {
                                let error = NSError(
                                    domain: "AppErrorDomain",
                                    code: -1,
                                    userInfo: [
                                        NSLocalizedDescriptionKey: "Unable to retrieve population from snapshot \(sfDocument)"
                                    ]
                                )
                                errorPointer?.pointee = error
                                return nil
                            }
                            transaction.updateData(["boosts": oldBoosts + 180], forDocument: sfReference)
                            return nil
                        }) { (object, error) in
                            if let error = error {
                                print("Transaction failed: \(error)")
                            } else {
                                let newBoosts = UserDefaults.standard.integer(forKey: "boosts") + 180
                                UserDefaults.standard.set(newBoosts, forKey: "boosts")
                                self.showNotification(title: "Success!", message: "Thank you for supporting and purchasing \(180) boosts. You now have \(newBoosts) boosts! 🚀")
                                Analytics.logEvent("did_purchase", parameters: [
                                    AnalyticsParameterPrice: 9.99
                                    ])
                            }
                        }
                    } else if productId == "4" {
                        let db = Firestore.firestore()
                        let sfReference = db.collection("users").document(Auth.auth().currentUser!.uid)
                        
                        db.runTransaction({ (transaction, errorPointer) -> Any? in
                            let sfDocument: DocumentSnapshot
                            do {
                                try sfDocument = transaction.getDocument(sfReference)
                            } catch let fetchError as NSError {
                                errorPointer?.pointee = fetchError
                                return nil
                            }
                            
                            guard let oldBoosts = sfDocument.data()?["boosts"] as? Int else {
                                let error = NSError(
                                    domain: "AppErrorDomain",
                                    code: -1,
                                    userInfo: [
                                        NSLocalizedDescriptionKey: "Unable to retrieve population from snapshot \(sfDocument)"
                                    ]
                                )
                                errorPointer?.pointee = error
                                return nil
                            }
                            transaction.updateData(["boosts": oldBoosts + 420], forDocument: sfReference)
                            return nil
                        }) { (object, error) in
                            if let error = error {
                                print("Transaction failed: \(error)")
                            } else {
                                let newBoosts = UserDefaults.standard.integer(forKey: "boosts") + 420
                                UserDefaults.standard.set(newBoosts, forKey: "boosts")
                                self.showNotification(title: "Success!", message: "Thank you for supporting and purchasing \(420) boosts. You now have \(newBoosts) boosts! 🚀")
                                Analytics.logEvent("did_purchase", parameters: [
                                    AnalyticsParameterPrice: 19.99
                                    ])
                            }
                        }
                    }
                break
                default: break
                }
            }
        }
    }
    
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    //IAP variabels
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var productId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get SKProducts
        self.fetchAvailableProducts()
        
        //attempt to load rewarded video
        //test: ca-app-pub-3940256099942544/1712485313
        //real: ca-app-pub-7147612881925877/9480766408
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        let request = GADRequest()
        GADRewardBasedVideoAd.sharedInstance().load(request,
                                                    withAdUnitID: "ca-app-pub-7147612881925877/9480766408")
        
        //semi transparent background
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        //rounded tops
        contentView.roundCorners([.topLeft, .topRight], radius: 20)

        //set up swipe gesture
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        //style buttons
        button1.layer.cornerRadius = 15
        button2.layer.cornerRadius = 15
        button3.layer.cornerRadius = 15
        button4.layer.cornerRadius = 15
        button5.layer.cornerRadius = 15
        
        //set up gradient on last button
        let gradient = CAGradientLayer()
        gradient.frame = button5.bounds
        gradient.colors = [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor, UIColor(red:0.88, green:0.27, blue:0.58, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.cornerRadius = 15
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        button5.layer.insertSublayer(gradient, at: 0)
    }
    
    @IBAction func freeTier(_ sender: Any) {
        
        if GADRewardBasedVideoAd.sharedInstance().isReady == true {
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
        } else {
            self.showNotification(title: "Oops", message: "No ads to show at this time, please try again.")
        }
    }
    
    @IBAction func tier1(_ sender: Any) {
        self.purchaseMyProduct(product: iapProducts[0])
    }
    
    @IBAction func tier2(_ sender: Any) {
        self.purchaseMyProduct(product: iapProducts[1])
    }
    
    @IBAction func tier3(_ sender: Any) {
        self.purchaseMyProduct(product: iapProducts[2])
    }
    
    @IBAction func tier4(_ sender: Any) {
        self.purchaseMyProduct(product: iapProducts[3])
    }
    

    @objc func handleGesture() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //ADMOB REWARDED ADS DELEGATE METHODS
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        let db = Firestore.firestore()
        let sfReference = db.collection("users").document(Auth.auth().currentUser!.uid)
        
        db.runTransaction({ (transaction, errorPointer) -> Any? in
            let sfDocument: DocumentSnapshot
            do {
                try sfDocument = transaction.getDocument(sfReference)
            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }
            
            guard let oldBoosts = sfDocument.data()?["boosts"] as? Int else {
                let error = NSError(
                    domain: "AppErrorDomain",
                    code: -1,
                    userInfo: [
                        NSLocalizedDescriptionKey: "Unable to retrieve population from snapshot \(sfDocument)"
                    ]
                )
                errorPointer?.pointee = error
                return nil
            }
            transaction.updateData(["boosts": oldBoosts + 2], forDocument: sfReference)
            return nil
        }) { (object, error) in
            if let error = error {
                print("Transaction failed: \(error)")
            } else {
                let newBoosts = UserDefaults.standard.integer(forKey: "boosts") + 2
                UserDefaults.standard.set(newBoosts, forKey: "boosts")
                self.showNotification(title: "Success!", message: "You just got \(2) free boosts. You now have \(newBoosts) boosts! 🚀")
                Analytics.logEvent("rewarded_ad", parameters: nil)
            }
        }
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidCompletePlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad has completed.")
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        let request = GADRequest()
        GADRewardBasedVideoAd.sharedInstance().load(request,
                                                    withAdUnitID: "ca-app-pub-7147612881925877/9480766408")
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
    }
    
    func showNotification(title: String, message: String) {
        var attributes = EKAttributes.topFloat
        attributes.entryBackground = .gradient(gradient: .init(colors: [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 15, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.roundCorners = .all(radius: 15)
        attributes.displayDuration = 2
        let minEdge = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: minEdge), height: .intrinsic)
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont(name: "CircularStd-Bold", size: 15)!, color: UIColor.white))
        let description = EKProperty.LabelContent(text: message, style: .init(font: UIFont(name: "CircularStd-Book", size: 14)!, color: UIColor.white))
        let image = EKProperty.ImageContent(image: UIImage(named: "CasperIcon")!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    func purchaseMyProduct(product: SKProduct) {
        if self.canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            
            self.productId = product.productIdentifier
            
            
            // IAP Purchases dsabled on the Device
        } else {
            self.showNotification(title: "Oops", message: "Purchases are disabled on this device.")
        }
    }
    
    func fetchAvailableProducts()  {
        
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects:
            "1","2","3","4"
        )
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }


}
