//
//  ProfileVC.swift
//  Casper
//
//  Created by John Leonardo on 1/15/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase
import SwiftEntryKit
import NVActivityIndicatorView

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //for image cache
    let imageCache = NSCache<NSString, UIImage>()
    
    let settings = [
        ["title": "Change Avatar"],
        ["title": "Change Username", "image": "id-card"],
        ["title": "Get More Boosts", "image": "rocket"],
        ["title": "Support Chat", "image": "support-speech-bubble"],
        ["title": "Backup Account", "image": "cloud-upload-arrow"],
        ["title": "Recover Account", "image": "download-from-cloud"],
        ["title": "Casper Rules", "image": "justice"],
        ["title": "Terms of Service", "image": "signing-contract"],
        ["title": "Privacy Policy", "image": "secure-file"],
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        //gradient view
        let gradient = CAGradientLayer()
        gradient.frame = topView.bounds
        gradient.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        topView.layer.insertSublayer(gradient, at: 0)
        //gradient top view
        let gradient2 = CAGradientLayer()
        gradient2.frame = view.bounds
        gradient2.colors = [UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor, UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor]
        gradient2.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient2.endPoint = CGPoint(x: 1.0, y: 0.5)
        view.layer.insertSublayer(gradient2, at: 0)
        
        //set data source + delegate
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        let currentSetting = self.settings[indexPath.row]
        cell.label.text = currentSetting["title"]!
        if indexPath.row == 0, let uid = Auth.auth().currentUser?.uid {
            //attempt to load image from cache
            if let imageFromCache = imageCache.object(forKey: uid as! NSString) {
                cell.icon.image = imageFromCache
            } else {
                //attempt to download from storage
                let storage = Storage.storage().reference(withPath: "avatars/" + uid)
                storage.getData(maxSize: 1 * 1024 * 1024) { data, error in
                    if let error = error {
                        //probably doesn't exist, load random default profile pic
                        let num = Int.random(in: 1...12)
                        cell.icon.image = UIImage(named: "DefaultAvatar\(num)")
                        self.imageCache.setObject(UIImage(named: "DefaultAvatar\(num)")!, forKey: uid as NSString)
                    } else {
                        // Data for "images/island.jpg" is returned
                        cell.icon.image = UIImage(data: data!)
                        self.imageCache.setObject(UIImage(data: data!)!, forKey: uid as! NSString)
                    }
                }
            }
        } else {
            cell.icon.image = UIImage(named: currentSetting["image"]!)!
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //change avatar
            let vc = UIImagePickerController()
            vc.sourceType = .photoLibrary
            vc.allowsEditing = true
            vc.delegate = self
            present(vc, animated: true)
        } else if indexPath.row == 1 {
            //change username
            let alertController = UIAlertController(title: "Change username", message: "Type the new username you would like.", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Username"
            }
            let addAction = UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
                let usernameTextField = alertController.textFields![0] as UITextField
                
                if usernameTextField.text != "", let uid = Auth.auth().currentUser?.uid {
                    if usernameTextField.text!.isAlphanumeric && usernameTextField.text!.count < 20 {
                        let db = Firestore.firestore()
                        let usernameQuery = db.collection("usernames").whereField("username", isEqualTo: usernameTextField.text!.lowercased())
                        usernameQuery.getDocuments(completion: { (snapshot, error) in
                            if let err = error {
                                self.showNotification(title: "Error", message: err.localizedDescription)
                            } else if let res = snapshot {
                                if res.documents.count == 0 {
                                    //no results, means username doesn't exist
                                    
                                    //create batch reference
                                    let batch = db.batch()
                                    let ref1 = db.collection("users").document(uid)
                                    let ref2 = db.collection("usernames").document(uid)
                                    
                                    batch.updateData(["username": usernameTextField.text!.lowercased()], forDocument: ref1)
                                    batch.updateData(["username": usernameTextField.text!.lowercased()], forDocument: ref2)
                                    
                                    //commit the batch
                                    batch.commit() { err in
                                        if let err = err {
                                            print("Error writing batch \(err)")
                                        } else {
                                            print("Batch write succeeded.")
                                            UserDefaults.standard.set(usernameTextField.text!.lowercased(), forKey: "username")
                                        }
                                    }
                                    
                                } else {
                                    //username exists
                                    self.showNotification(title: "Oops", message: "@\(usernameTextField.text!) is already taken.")
                                }
                            }
                        })
                    } else {
                        self.showNotification(title: "Oops", message: "Usernames can only contain letters and numbers, and must be under 20 characters in length.")
                    }
                    
                } else {
                    self.showNotification(title: "Error", message: "Text field can't be empty.")
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alertController.addAction(addAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        } else if indexPath.row == 6 {
            //rules
            guard let url = URL(string: "https://medium.com/@casperchat/community-rules-d22104ebf8c7") else { return }
            UIApplication.shared.open(url)
        } else if indexPath.row == 7 {
            //terms
            guard let url = URL(string: "https://medium.com/@casperchat/terms-of-service-43b6d3e8c4fe") else { return }
            UIApplication.shared.open(url)
        } else if indexPath.row == 8 {
            //privacy
            guard let url = URL(string: "https://medium.com/@casperchat/privacy-policy-9fdc13d57e6d") else { return }
            UIApplication.shared.open(url)
        } else if indexPath.row == 3 {
            self.performSegue(withIdentifier: "goToSupportChat", sender: nil)
        } else if indexPath.row == 2 {
            self.performSegue(withIdentifier: "goToBoostsModal2", sender: nil)
        } else if indexPath.row == 4 {
            self.showNotification(title: "Coming soon!", message: "This feature is coming soon.")
        } else if indexPath.row == 5 {
            self.showNotification(title: "Coming soon!", message: "This feature is coming soon.")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found.")
            return
        }
        
        startAnimating(CGSize(width: 30, height: 30), message: "Uploading photo...", type: NVActivityIndicatorType.allCases.randomElement())
        
        //set new image in cache
        imageCache.setObject(image, forKey: Auth.auth().currentUser!.uid as NSString)
        let storage = Storage.storage().reference()
        let avatarRef = storage.child("/avatars/" + Auth.auth().currentUser!.uid)
        let data = image.jpegData(compressionQuality: 0.3)
        //upload data to user path
        let uploadTask = avatarRef.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                //uh-oh, an error occurred!
                return
            }
            
            self.stopAnimating()
            
        }
        self.tableView.reloadData()
    }
    
    func showNotification(title: String, message: String) {
        var attributes = EKAttributes.topFloat
        attributes.entryBackground = .gradient(gradient: .init(colors: [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 15, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.roundCorners = .all(radius: 15)
        attributes.displayDuration = 2
        let minEdge = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: minEdge), height: .intrinsic)
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont(name: "CircularStd-Bold", size: 15)!, color: UIColor.white))
        let description = EKProperty.LabelContent(text: message, style: .init(font: UIFont(name: "CircularStd-Book", size: 14)!, color: UIColor.white))
        let image = EKProperty.ImageContent(image: UIImage(named: "CasperIcon")!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }

}
