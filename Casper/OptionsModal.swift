//
//  OptionsModal.swift
//  Casper
//
//  Created by John Leonardo on 1/16/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit
import Firebase

class OptionsModal: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var buttonBar1: UIView!
    @IBOutlet weak var buttonBar2: UIView!
    
    @IBOutlet weak var guysBtn: UIButton!
    @IBOutlet weak var girlsBtn: UIButton!
    @IBOutlet weak var bothBtn: UIButton!
    
    @IBOutlet weak var recentBtn: UIButton!
    @IBOutlet weak var randomBtn: UIButton!
    
    @IBOutlet weak var boostBtn: UIButton!
    
    @IBOutlet weak var boostsLbl: UILabel!
    
    var genderPref = 0
    var filterPref = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        //round bottom corners of content view
        contentView.roundCorners([.bottomLeft, .bottomRight], radius: 15)
        
        //style button bar content holders
        buttonBar1.layer.borderColor = UIColor.lightGray.cgColor
        buttonBar1.layer.borderWidth = 1
        buttonBar1.layer.cornerRadius = 10
        buttonBar2.layer.borderColor = UIColor.lightGray.cgColor
        buttonBar2.layer.borderWidth = 1
        buttonBar2.layer.cornerRadius = 10
        
        //style buttons
        guysBtn.layer.cornerRadius = 10
        girlsBtn.layer.cornerRadius = 10
        bothBtn.layer.cornerRadius = 10
        recentBtn.layer.cornerRadius = 10
        randomBtn.layer.cornerRadius = 10
        
        //style boost btn
        boostBtn.clipsToBounds = false
        boostBtn.layer.cornerRadius = 10
        boostBtn.layer.shadowColor = UIColor.black.cgColor
        boostBtn.layer.shadowOpacity = 0.5
        boostBtn.layer.shadowOffset = CGSize.zero
        boostBtn.layer.shadowRadius = 10
        boostBtn.layer.shadowPath = UIBezierPath(roundedRect: boostBtn.bounds, cornerRadius: 10).cgPath
        
        //load settings
        loadSettings()
        
        //set boosts label
        self.boostsLbl.text = "🚀  \(UserDefaults.standard.integer(forKey: "boosts"))"
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        UserDefaults.standard.set(genderPref, forKey: "gender")
        UserDefaults.standard.set(filterPref, forKey: "filter")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func guys(_ sender: Any) {
        self.genderPref = 2
        guysBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
        guysBtn.layer.borderWidth = 1
        guysBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
        
        girlsBtn.layer.borderColor = UIColor.clear.cgColor
        girlsBtn.layer.borderWidth = 0
        girlsBtn.setTitleColor(UIColor.black, for: .normal)
        
        bothBtn.layer.borderColor = UIColor.clear.cgColor
        bothBtn.layer.borderWidth = 0
        bothBtn.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func girls(_ sender: Any) {
        self.genderPref = 1
        girlsBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
        girlsBtn.layer.borderWidth = 1
        girlsBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
        
        guysBtn.layer.borderColor = UIColor.clear.cgColor
        guysBtn.layer.borderWidth = 0
        guysBtn.setTitleColor(UIColor.black, for: .normal)
        
        bothBtn.layer.borderColor = UIColor.clear.cgColor
        bothBtn.layer.borderWidth = 0
        bothBtn.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func both(_ sender: Any) {
        self.genderPref = 0
        bothBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
        bothBtn.layer.borderWidth = 1
        bothBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
        
        girlsBtn.layer.borderColor = UIColor.clear.cgColor
        girlsBtn.layer.borderWidth = 0
        girlsBtn.setTitleColor(UIColor.black, for: .normal)
        
        guysBtn.layer.borderColor = UIColor.clear.cgColor
        guysBtn.layer.borderWidth = 0
        guysBtn.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func recent(_ sender: Any) {
        self.filterPref = 0
        recentBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
        recentBtn.layer.borderWidth = 1
        recentBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
        
        randomBtn.layer.borderColor = UIColor.clear.cgColor
        randomBtn.layer.borderWidth = 0
        randomBtn.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func random(_ sender: Any) {
        self.filterPref = 1
        randomBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
        randomBtn.layer.borderWidth = 1
        randomBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
        
        recentBtn.layer.borderColor = UIColor.clear.cgColor
        recentBtn.layer.borderWidth = 0
        recentBtn.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func boost(_ sender: Any) {
        //boost
        if UserDefaults.standard.integer(forKey: "boosts") > 0 {
            let alertController = UIAlertController(title: "Boost", message: "Do you want to boost now? This will bump your profile to the top of the recents section. You will have \(UserDefaults.standard.integer(forKey: "boosts") - 1) boosts after this.", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Boost", style: .default) { (action) in
                //has sufficient balance
                let db = Firestore.firestore()
                let sfReference = db.collection("users").document(Auth.auth().currentUser!.uid)
                
                db.runTransaction({ (transaction, errorPointer) -> Any? in
                    let sfDocument: DocumentSnapshot
                    do {
                        try sfDocument = transaction.getDocument(sfReference)
                    } catch let fetchError as NSError {
                        errorPointer?.pointee = fetchError
                        return nil
                    }
                    
                    guard let oldBoosts = sfDocument.data()?["boosts"] as? Int else {
                        let error = NSError(
                            domain: "AppErrorDomain",
                            code: -1,
                            userInfo: [
                                NSLocalizedDescriptionKey: "Unable to retrieve population from snapshot \(sfDocument)"
                            ]
                        )
                        errorPointer?.pointee = error
                        return nil
                    }
                    
                    if oldBoosts <= 0 {
                        self.showInsufficientBalanceError()
                        return nil
                    }
                    transaction.updateData(["boosts": oldBoosts - 1], forDocument: sfReference)
                    return nil
                }) { (object, error) in
                    if let error = error {
                        print("Transaction failed: \(error)")
                    } else {
                        let newBoosts = UserDefaults.standard.integer(forKey: "boosts") - 1
                        UserDefaults.standard.set(newBoosts, forKey: "boosts")
                        self.boostsLbl.text = "🚀  \(newBoosts)"
                        let userRef = db.collection("recents").document(Auth.auth().currentUser!.uid)
                        userRef.setData(["timestamp": Timestamp.init(), "r": Int.random(in: Int.min ... Int.max)], merge: true)
                    }
                }
            }
            
            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                //cancel
            }
            
            alertController.addAction(action1)
            alertController.addAction(action2)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            //doesn't have sufficient balance
            self.showInsufficientBalanceError()
        }
    }
    
    func showInsufficientBalanceError() {
        let alertController = UIAlertController(title: "Error", message: "Oops, no more boosts left. You can watch a short video advertisement to get two boosts, or buy some in the shop.", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Get boosts", style: .default) { (action:UIAlertAction) in
            //open boosts modal
            //UserDefaults.standard.set(20, forKey: "boosts")
            self.performSegue(withIdentifier: "goToBoostsModal", sender: nil)
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            //cancel
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func loadSettings() {
        let genderPreference = UserDefaults.standard.integer(forKey: "gender")
        let filterPreference = UserDefaults.standard.integer(forKey: "filter")
        
        genderPref = genderPreference
        filterPref = filterPreference
        
        if genderPreference == 0 {
            bothBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
            bothBtn.layer.borderWidth = 1
            bothBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
            
            girlsBtn.layer.borderColor = UIColor.clear.cgColor
            girlsBtn.layer.borderWidth = 0
            girlsBtn.setTitleColor(UIColor.black, for: .normal)
            
            guysBtn.layer.borderColor = UIColor.clear.cgColor
            guysBtn.layer.borderWidth = 0
            guysBtn.setTitleColor(UIColor.black, for: .normal)
        } else if genderPreference == 1 {
            girlsBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
            girlsBtn.layer.borderWidth = 1
            girlsBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
            
            guysBtn.layer.borderColor = UIColor.clear.cgColor
            guysBtn.layer.borderWidth = 0
            guysBtn.setTitleColor(UIColor.black, for: .normal)
            
            bothBtn.layer.borderColor = UIColor.clear.cgColor
            bothBtn.layer.borderWidth = 0
            bothBtn.setTitleColor(UIColor.black, for: .normal)
        } else if genderPreference == 2 {
            guysBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
            guysBtn.layer.borderWidth = 1
            guysBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
            
            girlsBtn.layer.borderColor = UIColor.clear.cgColor
            girlsBtn.layer.borderWidth = 0
            girlsBtn.setTitleColor(UIColor.black, for: .normal)
            
            bothBtn.layer.borderColor = UIColor.clear.cgColor
            bothBtn.layer.borderWidth = 0
            bothBtn.setTitleColor(UIColor.black, for: .normal)
        }
        
        if filterPreference == 0 {
            recentBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
            recentBtn.layer.borderWidth = 1
            recentBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
            
            randomBtn.layer.borderColor = UIColor.clear.cgColor
            randomBtn.layer.borderWidth = 0
            randomBtn.setTitleColor(UIColor.black, for: .normal)
        } else if filterPreference == 1 {
            randomBtn.layer.borderColor = UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor
            randomBtn.layer.borderWidth = 1
            randomBtn.setTitleColor(UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0), for: .normal)
            
            recentBtn.layer.borderColor = UIColor.clear.cgColor
            recentBtn.layer.borderWidth = 0
            recentBtn.setTitleColor(UIColor.black, for: .normal)
        }
    }

}
