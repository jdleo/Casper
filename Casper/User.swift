//
//  User.swift
//  Casper
//
//  Created by John Leonardo on 2/2/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import MessengerKit

struct User: MSGUser {
    
    var displayName: String
    
    var avatar: UIImage?
    
    var avatarUrl: URL?
    
    var isSender: Bool
    
}
