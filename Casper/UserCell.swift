//
//  UserCell.swift
//  Casper
//
//  Created by John Leonardo on 1/16/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//

import UIKit

class UserCell: UICollectionViewCell {
    
    @IBOutlet weak var innerContentView: UIView!
    @IBOutlet weak var avatarImg: UIImageView!
    
}
