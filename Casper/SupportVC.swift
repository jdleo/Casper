//
//  SupportVC.swift
//  Casper
//
//  Created by John Leonardo on 2/2/19.
//  Copyright © 2019 John Leonardo. All rights reserved.
//
import UIKit
import MessengerKit
import Firebase

class SupportVC: MSGMessengerViewController {
    let steve = User(displayName: UserDefaults.standard.object(forKey: "username") as! String, avatar: UIImage(named: "DefaultAvatar6")!, avatarUrl: nil, isSender: true)
    
    let casper = User(displayName: "Casper", avatar: UIImage(named: "DefaultAvatar1")!, avatarUrl: nil, isSender: false)
    
    var id = 100
    
    override var style: MSGMessengerStyle {
        var style = MessengerKit.Styles.travamigos
        style.outgoingGradient = [UIColor(red:0.56, green:0.33, blue:0.91, alpha:1.0).cgColor, UIColor(red:0.40, green:0.40, blue:0.91, alpha:1.0).cgColor, UIColor(red:0.28, green:0.46, blue:0.90, alpha:1.0).cgColor]
        return style
    }
    
    
    var seen: [String] = []
    
    lazy var messages: [[MSGMessage]] = {
        return [[MSGMessage(id: 100, body: MSGMessageBody.text("Welcome to Casper Support Chat! Feel free to ask us anything, and we'll get back to you as quickly as possible ☺️"), user: self.casper, sentAt: Date(timeIntervalSince1970: 100.0))]]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Support Chat"
        
        dataSource = self
        delegate = self
        
        let db = Firestore.firestore()
        
        //get existing support messages
        let chatRef = db.collection("support").document(Auth.auth().currentUser!.uid).collection("messages").order(by: "timestamp")
        
        //listen for changes
        chatRef.addSnapshotListener { (snapshot, error) in
            
            guard let documents = snapshot?.documents else {
                print("Error fetching documents: \(error!)")
                return
            }
            
            for document in documents {
                if !self.seen.contains(document.documentID) {
                    self.id += 1
                    
                    let body = document.data()["body"] as! String
                    let sender = document.data()["sender"] as! String
                    let timestamp = document.data()["timestamp"] as! Timestamp
                    
                    if body.containsOnlyEmoji {
                        //is emoji text
                        if sender == Auth.auth().currentUser!.uid {
                            //is self
                            self.insert(MSGMessage(id: self.id, body: .emoji(body), user: self.steve, sentAt: timestamp.dateValue()))
                        } else {
                            //is casper
                            self.insert(MSGMessage(id: self.id, body: .emoji(body), user: self.casper, sentAt: timestamp.dateValue()))
                        }
                    } else {
                        //is regular text
                        if sender == Auth.auth().currentUser!.uid {
                            //is self
                            self.insert(MSGMessage(id: self.id, body: .text(body), user: self.steve, sentAt: timestamp.dateValue()))
                        } else {
                            //is casper
                            self.insert(MSGMessage(id: self.id, body: .text(body), user: self.casper, sentAt: timestamp.dateValue()))
                        }
                    }
                    
                    self.seen.append(document.documentID)
                }
            }
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        collectionView.scrollToBottom(animated: false)
        
    }
    
    override func inputViewPrimaryActionTriggered(inputView: MSGInputView) {

        
        let db = Firestore.firestore()
        let supportChatRef = db.collection("support").document(Auth.auth().currentUser!.uid).collection("messages")
        
        supportChatRef.addDocument(data: [
            "body": inputView.message,
            "timestamp": Timestamp.init(),
            "sender": Auth.auth().currentUser!.uid
        ])
        
        inputView.resignFirstResponder()
    }
    
    override func insert(_ message: MSGMessage) {
        
        collectionView.performBatchUpdates({
            if let lastSection = self.messages.last, let lastMessage = lastSection.last, lastMessage.user.displayName == message.user.displayName {
                self.messages[self.messages.count - 1].append(message)
                
                let sectionIndex = self.messages.count - 1
                let itemIndex = self.messages[sectionIndex].count - 1
                self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                
            } else {
                self.messages.append([message])
                let sectionIndex = self.messages.count - 1
                self.collectionView.insertSections([sectionIndex])
            }
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: true)
            self.collectionView.layoutTypingLabelIfNeeded()
        })
        
    }
    
    override func insert(_ messages: [MSGMessage], callback: (() -> Void)? = nil) {
        
        collectionView.performBatchUpdates({
            for message in messages {
                if let lastSection = self.messages.last, let lastMessage = lastSection.last, lastMessage.user.displayName == message.user.displayName {
                    self.messages[self.messages.count - 1].append(message)
                    
                    let sectionIndex = self.messages.count - 1
                    let itemIndex = self.messages[sectionIndex].count - 1
                    self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                    
                } else {
                    self.messages.append([message])
                    let sectionIndex = self.messages.count - 1
                    self.collectionView.insertSections([sectionIndex])
                }
            }
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: false)
            self.collectionView.layoutTypingLabelIfNeeded()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                callback?()
            }
        })
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

// MARK: - MSGDataSource
extension SupportVC: MSGDataSource {
    
    func numberOfSections() -> Int {
        return messages.count
    }
    
    func numberOfMessages(in section: Int) -> Int {
        return messages[section].count
    }
    
    func message(for indexPath: IndexPath) -> MSGMessage {
        return messages[indexPath.section][indexPath.item]
    }
    
    func footerTitle(for section: Int) -> String? {
        return "Just now"
    }
    
    func headerTitle(for section: Int) -> String? {
        return messages[section].first?.user.displayName
    }
    
}

// MARK: - MSGDelegate
extension SupportVC: MSGDelegate {
    
    func linkTapped(url: URL) {
        print("Link tapped:", url)
    }
    
    func avatarTapped(for user: MSGUser) {
        print("Avatar tapped:", user)
    }
    
    func tapReceived(for message: MSGMessage) {
        print("Tapped: ", message)
    }
    
    func longPressReceieved(for message: MSGMessage) {
        print("Long press:", message)
    }
    
    func shouldDisplaySafari(for url: URL) -> Bool {
        return true
    }
    
    func shouldOpen(url: URL) -> Bool {
        return true
    }
    
}
